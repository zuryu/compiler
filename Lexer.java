import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;

///////////////////////////////////////////////////////////////////////////////////////////////

class LexerImpl implements Lexer {
    @Override
    public List<Token> lex (String input) throws LexicalException, Task1Exception {
        List<Token> tokens = new LinkedList<>();
        int inputIndex = 0;
        char inputChar;
        while (inputIndex < input.length()){            
            inputChar = input.charAt(inputIndex);
            if (Character.isLetter(inputChar)){
                String longestString = getLongestString(inputIndex, input);
                tokens.add(checkKeywordOrId(inputChar, longestString));
                inputIndex += longestString.length();
            } else if (Character.isDigit(inputChar)){
                String longestDigit = getLongestDigit(inputIndex, input);
                tokens.add(new T_Integer(Integer.parseInt(longestDigit)));
                inputIndex += longestDigit.length();
            } else {
                switch(inputChar) {
                    case ';':
                        tokens.add(new T_Semicolon());
                        inputIndex++;
                        break;
                    case '(':
                        tokens.add(new T_LeftBracket());
                        inputIndex++;
                        break;
                    case ')':
                        tokens.add(new T_RightBracket());
                        inputIndex++;
                        break;
                    case '=':
                        String longestEquals = checkEquals(inputIndex, input);
                        if (longestEquals.length() == 2){
                            tokens.add(new T_Equal());
                        } else {
                            tokens.add(new T_EqualDefines());
                        }
                        inputIndex += longestEquals.length();
                        break;
                    case '<':
                        String longestLT = checkEquals(inputIndex, input);
                        if (longestLT.length() == 2){
                            tokens.add(new T_LessEq());
                        } else {
                            tokens.add(new T_LessThan());
                        }
                        inputIndex += longestLT.length();
                        break;
                    case '>':
                        String longestGT = checkEquals(inputIndex, input);
                        if (longestGT.length() == 2){
                            tokens.add(new T_GreaterEq());
                        } else {
                            tokens.add(new T_GreaterThan());
                        }
                        inputIndex += longestGT.length();
                        break;
                    case ',':
                        tokens.add(new T_Comma());
                        inputIndex++;
                        break;
                    case '{':
                        tokens.add(new T_LeftCurlyBracket());
                        inputIndex++;
                        break;
                    case '}':
                        tokens.add(new T_RightCurlyBracket());
                        inputIndex++;
                        break;
                    case ':':
                        if ((input.length() > inputIndex + 1) && input.charAt(inputIndex + 1) == '='){
                            tokens.add(new T_Assign());
                            inputIndex += 2;
                        } else {
                            throw new LexicalException("No '=' after ':' in assignment; lex");
                        }
                        break;
                    case '+':
                        tokens.add(new T_Plus());
                        inputIndex++;
                        break;
                    case '-':
                        tokens.add(new T_Minus());
                        inputIndex++;
                        break;
                    case '*':
                        tokens.add(new T_Times());
                        inputIndex++;
                        break;
                    case '/':
                        tokens.add(new T_Div());
                        inputIndex++;
                        break;
                    case ' ': 
                    case '\n': 
                    case '\f': 
                    case '\r': 
                    case '\t': 
                    case (char)11:
                        inputIndex++;
                        break;
                    default:
                        throw new LexicalException("Unrecognised character; lex: " + inputChar);
                }
            }
        }
        return tokens;
    }
    
    public String getLongestString(int index, String input){
        char currentChar;
        int currentIndex = index;
        String longPart = "";
        do {
            currentChar = input.charAt(currentIndex);
            longPart = longPart.concat(Character.toString(currentChar));
            currentIndex++;
        } while (currentIndex < input.length() && (Character.isLetterOrDigit(input.charAt(currentIndex)) || input.charAt(currentIndex) == '_'));
        return longPart;
    }
    
    public String getLongestDigit(int index, String input){
        char currentChar;
        int currentIndex = index;
        String longPart = "";
        do {
            currentChar = input.charAt(currentIndex);
            longPart = longPart.concat(Character.toString(currentChar));
            currentIndex++;
        } while (currentIndex < input.length() && (Character.isDigit(input.charAt(currentIndex))));
        return longPart;   
    }
    
    public String checkEquals(int index, String input){
        if ((input.length() > index + 1) && (input.charAt(index + 1) == '=')){
            return "" + input.charAt(index) + input.charAt(index + 1);
        } else {
            return String.valueOf(input.charAt(index));
        }
    }
    
    public Token checkKeywordOrId(char inputChar, String longestString) throws LexicalException {
        Token token;
        if (Character.isLowerCase(inputChar)){
                switch(longestString){
                    case "if":
                        token = new T_If();
                        break;
                    case "else":
                        token = new T_Else();
                        break;
                    case "def":
                        token = new T_Def();
                        break;
                    case "then":
                        token = new T_Then();
                        break;
                    case "skip":
                        token = new T_Skip();
                        break;
                    case "while":
                        token = new T_While();
                        break;
                    case "do":
                        token = new T_Do();
                        break;
                    case "repeat":
                        token = new T_Repeat();
                        break;
                    case "until":
                        token = new T_Until();
                        break;
                    case "break":
                        token = new T_Break();
                        break;
                    case "continue":
                        token = new T_Continue();
                        break;
                    default:
                        token = new T_Identifier(longestString);
                }
        } else {
            throw new LexicalException("Keyword or Identifier cannot start with an upper-case letter; checkKeywordOrId"); 
        }
        return token;
    }
}


///////////////////////////////////////////////////////////////////////////////////////////

class LexicalException extends Exception {
    public String msg;
    public LexicalException ( String _msg ) { msg = _msg; } } 

class Task1Exception extends Exception {
    public String msg;
    public Task1Exception ( String _msg ) { msg = _msg; } }

interface Lexer {
    public List<Token> lex ( String input ) throws LexicalException, Task1Exception; }

class Task1 {
    public static Lexer create () { return new LexerImpl (); } }

/////////////////////////////////////////////////////////////////////////////////////////////

interface Token {}

class T_Semicolon implements Token {} // represents ;
class T_LeftBracket implements Token {} // represents (
class T_RightBracket implements Token {} // represents )
class T_EqualDefines implements Token {} // represents =
class T_Equal implements Token {} // represents ==
class T_LessThan implements Token {} // represents < 
class T_GreaterThan implements Token {} // represents >
class T_LessEq implements Token {} // represents <=
class T_GreaterEq implements Token {} // represents >=
class T_Comma implements Token {} // represents ,
class T_LeftCurlyBracket implements Token {} // represents {
class T_RightCurlyBracket implements Token {} // represents }
class T_Assign implements Token {} // represents :=
class T_Plus implements Token {} // represents +
class T_Times implements Token {} // represents *
class T_Minus implements Token {} // represents -
class T_Div implements Token {} // represents /
class T_Identifier implements Token { // represents names like x, i, n, numberOfNodes ...
    public String s;
    public T_Identifier ( String _s ) { s = _s; } }
class T_Integer implements Token { // represents non-negative numbers like 0, 1, 2, 3, ...
    public int n;
    public T_Integer ( int _n ) { n = _n; } }
class T_Def implements Token {} // represents def
class T_Skip implements Token {} // represents skip
class T_If implements Token {} // represents if
class T_Then implements Token {} // represents then
class T_Else implements Token {} // represents else
class T_While implements Token {} // represents while
class T_Do implements Token {} // represents do
class T_Repeat implements Token {} // represents repeat
class T_Until implements Token {} // represents until
class T_Break implements Token {} // represents break
class T_Continue implements Token {} // represents Continue