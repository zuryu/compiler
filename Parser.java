import java.util.LinkedList;
import java.util.List;
import java.util.ArrayList;

///////////////////////////////////////////////////////////////////////////////////////////

class ParserImpl implements Parser {    

    public Block parse ( List<Token> input ) throws SyntaxException, Task2Exception {
        ArrayList<Token> inputCopy = new ArrayList<Token>();
        inputCopy.clear();
        inputCopy.addAll(input);
        ExpPair pair = parseBlock(inputCopy);
        List<Exp> expInner = pair.exps2;
        inputCopy.clear();
        inputCopy.addAll(pair.input2);
        if (!(inputCopy.isEmpty())){
            throw new SyntaxException("Invalid input : parse");
        }
        Block ast = new Block(expInner);
        return ast;
    }
    
    private ExpPair parseBlock(List<Token> input) throws SyntaxException {
        ArrayList<Token> inputList = new ArrayList<Token>();
        inputList.addAll(input);
        List<Exp> list = new ArrayList<Exp>();
        if (!inputList.isEmpty() && inputList.get(0) instanceof T_LeftCurlyBracket){
            inputList.remove(0); 
            ExpPair pair = parseENE(inputList);
            list.addAll(pair.exps2);
            inputList.clear();
            inputList.addAll(pair.input2);
            if (!inputList.isEmpty() && inputList.get(0) instanceof T_RightCurlyBracket){
                inputList.remove(0);
            } else {
                throw new SyntaxException("Invalid input : parseBlock");
            }
        } else {
            throw new SyntaxException("Blocks of code must start with { : parseBlock");
        }
        return new ExpPair(list, inputList);
    }
    
    private ExpPair parseENE(List<Token> input)throws SyntaxException {
        ArrayList<Token> inputList = new ArrayList<Token>();
        inputList.addAll(input);
        List<Exp> list = new ArrayList<Exp>();
        ExpPair pair = parseE(inputList);
        list.addAll(pair.exps2);
        inputList.clear();
        inputList.addAll(pair.input2);
        if (!inputList.isEmpty() && inputList.get(0) instanceof T_Semicolon){// check for range
            inputList.remove(0);
            ExpPair pairInner = parseENE(inputList);
            list.addAll(pairInner.exps2);
            inputList.clear();
            inputList.addAll(pairInner.input2);
            return new ExpPair(list, inputList);
        } else if (!inputList.isEmpty() && inputList.get(0) instanceof T_RightCurlyBracket){
            return new ExpPair(list, inputList);
        } else {
            throw new SyntaxException("Invalid input : parseENE");
        }
    }
    
    private ExpPair parseE(List<Token> input) throws SyntaxException {
        ArrayList<Token> inputList = new ArrayList<Token>();
        inputList.addAll(input);
        List<Exp> expInner = new ArrayList<Exp>();
        if (!inputList.isEmpty() && inputList.get(0) instanceof T_Integer){
            T_Integer number = (T_Integer)inputList.get(0);
            inputList.remove(0);
            expInner.add(new IntLiteral(number.n));
            return new ExpPair(expInner, inputList);
        } else if (!inputList.isEmpty() && inputList.get(0) instanceof T_Minus) {
            inputList.remove(0);
            if (!inputList.isEmpty() && inputList.get(0) instanceof T_Integer){
                T_Integer number = (T_Integer)inputList.get(0);
                inputList.remove(0);
                expInner.add(new IntLiteral(-number.n));
                return new ExpPair(expInner, inputList);
            } else {
                throw new SyntaxException("Minus must be followed by a number : parseE");
            }
        } else if (!inputList.isEmpty() && inputList.get(0) instanceof T_Skip){
            inputList.remove(0);
            expInner.add(new Skip());
            return new ExpPair(expInner, inputList);
        } else if (!inputList.isEmpty() && inputList.get(0) instanceof T_LeftCurlyBracket){
            ExpPair pair = parseBlock(inputList);
            inputList.clear();
            inputList.addAll(pair.input2);
            Block innerBlock = new Block(pair.exps2);
            BlockExp blockExp = new BlockExp(innerBlock);
            expInner.add(blockExp);
            return new ExpPair(expInner, inputList);
        } else {
            throw new SyntaxException("Invalid input : parseE");
        }
    }
}

class ExpPair {
    public List<Exp> exps2;
    public List<Token> input2;
    
    public ExpPair(List<Exp> exps2, List<Token> input2){
        this.exps2 = exps2;
        this.input2 = input2;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

class SyntaxException extends Exception {
    public String msg;
    public SyntaxException ( String _msg ) { msg = _msg; } }

class Task2Exception extends Exception {
    public String msg;
    public Task2Exception ( String _msg ) { msg = _msg; } }

interface Parser {
    public Block parse ( List<Token> input ) throws SyntaxException, 
                                                      Task2Exception; }

class Task2 {
    public static Parser create () { return new ParserImpl (); } }
	
/////////////////////////////////////////////////////////////////////////////////////////////

class Block {
    public List <Exp> exps;
    public Block ( List <Exp> _exps ) {
	assert ( _exps.size () > 0 );
	exps = _exps; } }

abstract class Exp {} 

class IntLiteral extends Exp { 
    public int n;
    IntLiteral ( int _n ) { n = _n; } }

class Skip extends Exp { 
    public Skip () {} }

class BlockExp extends Exp { 
    public Block b;
    public BlockExp ( Block _b ) { b = _b; } }

	
/////////////////////////////////////////////////////////////////////////////////////////////

interface Token {}

class T_Semicolon implements Token {} // represents ;
class T_LeftBracket implements Token {} // represents (
class T_RightBracket implements Token {} // represents )
class T_EqualDefines implements Token {} // represents =
class T_Equal implements Token {} // represents ==
class T_LessThan implements Token {} // represents < 
class T_GreaterThan implements Token {} // represents >
class T_LessEq implements Token {} // represents <=
class T_GreaterEq implements Token {} // represents >=
class T_Comma implements Token {} // represents ,
class T_LeftCurlyBracket implements Token {} // represents {
class T_RightCurlyBracket implements Token {} // represents }
class T_Assign implements Token {} // represents :=
class T_Plus implements Token {} // represents +
class T_Times implements Token {} // represents *
class T_Minus implements Token {} // represents -
class T_Div implements Token {} // represents /
class T_Identifier implements Token { // represents names like x, i, n, numberOfNodes ...
    public String s;
    public T_Identifier ( String _s ) { s = _s; } }
class T_Integer implements Token { // represents non-negative numbers like 0, 1, 2, 3, ...
    public int n;
    public T_Integer ( int _n ) { n = _n; } }
class T_Def implements Token {} // represents def
class T_Skip implements Token {} // represents skip
class T_If implements Token {} // represents if
class T_Then implements Token {} // represents then
class T_Else implements Token {} // represents else
class T_While implements Token {} // represents while
class T_Do implements Token {} // represents do
class T_Repeat implements Token {} // represents repeat
class T_Until implements Token {} // represents until
class T_Break implements Token {} // represents break
class T_Continue implements Token {} // represents Continue